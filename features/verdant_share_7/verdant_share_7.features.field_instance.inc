<?php
/**
 * @file
 * verdant_share_7.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function verdant_share_7_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-share_ride-field_arrival_time'
  $field_instances['node-share_ride-field_arrival_time'] = array(
    'bundle' => 'share_ride',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arrival_time',
    'label' => 'Arrival at Conference',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 1,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-share_ride-field_departure_time'
  $field_instances['node-share_ride-field_departure_time'] = array(
    'bundle' => 'share_ride',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_departure_time',
    'label' => 'Depart After Conference',
    'required' => 0,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 1,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-share_ride-field_description'
  $field_instances['node-share_ride-field_description'] = array(
    'bundle' => 'share_ride',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Describe your trip and maybe yourself.  If you don\\\'t want to be contacted with the contact form, how should people reach you?  Do you smoke, hate smoke?  Listen to loud music?  Drive fast, drive safe?  Gas money, help driving?',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 4,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-share_ride-field_ride_request'
  $field_instances['node-share_ride-field_ride_request'] = array(
    'bundle' => 'share_ride',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ride_request',
    'label' => 'Ride Request',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-means_of_transit-field_term_relation'
  $field_instances['taxonomy_term-means_of_transit-field_term_relation'] = array(
    'bundle' => 'means_of_transit',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Relations to other terms',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_relation',
    'label' => 'Related terms',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-means_of_transit-field_term_synonym'
  $field_instances['taxonomy_term-means_of_transit-field_term_synonym'] = array(
    'bundle' => 'means_of_transit',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Synonyms of this term',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_synonym',
    'label' => 'Synonyms',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Arrival at Conference');
  t('Depart After Conference');
  t('Describe your trip and maybe yourself.  If you don\\\'t want to be contacted with the contact form, how should people reach you?  Do you smoke, hate smoke?  Listen to loud music?  Drive fast, drive safe?  Gas money, help driving?');
  t('Description');
  t('Related terms');
  t('Relations to other terms');
  t('Ride Request');
  t('Synonyms');
  t('Synonyms of this term');

  return $field_instances;
}
